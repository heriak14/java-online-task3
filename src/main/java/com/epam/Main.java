package com.epam;

import com.epam.animals.AnimalManager;
import com.epam.shapes.ShapeManager;

public class Main {
    public static void main(String[] args) {
        AnimalManager.run();
        ShapeManager.run();
    }
}
