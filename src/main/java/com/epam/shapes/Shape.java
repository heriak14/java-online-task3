package com.epam.shapes;

public abstract class Shape {
    private int numOfAngles;

    public Shape(int numOfAngles) {
        this.numOfAngles = numOfAngles;
    }

    public Shape(){
        this.numOfAngles = 1;
    }

    public abstract double getSquare();
    public abstract double getPerimeter();
}
