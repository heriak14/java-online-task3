package com.epam.shapes;

public class Circle extends Shape {
    private int radius;

    public Circle(int radius) {
        if (radius > 0) {
            this.radius = radius;
        }
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        if (radius > 0) {
            this.radius = radius;
        }
    }

    public double getSquare() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
}
