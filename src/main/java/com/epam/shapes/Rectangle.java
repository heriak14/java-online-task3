package com.epam.shapes;

public class Rectangle extends Shape {
    private int a;      //first side
    private int b;      //second side
    private boolean isSquare;

    {

    }
    public Rectangle(int a, int b) {
        super(4);
        if ((a > 0) && (b > 0)) {
            this.a = a;
            this.b = b;
        }
        if (a == b) {
            isSquare = true;
        }
    }

    public double getSquare() {
        return a * b;
    }

    public double getPerimeter() {
        return (a + b)*2;
    }

    public boolean isSquare() {
        return isSquare;
    }
}
