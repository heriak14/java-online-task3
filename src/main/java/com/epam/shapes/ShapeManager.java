package com.epam.shapes;

import java.util.Scanner;

public class ShapeManager {
    private static Shape shape;
    private static Scanner sc ;

    static {
        sc = new Scanner(System.in);
        System.out.println("Enter № of shape what you want to see info about:");
        System.out.println("Rectangle - 1\nTriangle - 2\nCircle - 3");
        switch (sc.nextInt()) {
            case 1:
                shape = initRectangle();
                break;
            case 2:
                shape = initTriangle();
                break;
            case 3:
                shape = initCircle();
                break;
                default :
                    System.out.println("You have intered incorect data!");
                    System.exit(0);

        }
    }

    public static void run() {
        System.out.println("**********************************************");
        System.out.println("Your shape is " + shape.getClass().getSimpleName());
        System.out.println("Square = " + shape.getSquare());
        System.out.println("Perimetr = " + shape.getPerimeter());

        if (shape instanceof Rectangle) {
            if (((Rectangle) shape).isSquare()){
                System.out.println("Your rectangle is square!");
            }
        }
        if (shape instanceof Triangle) {
            System.out.println("Height = " + ((Triangle) shape).getHeight());
        }
        System.out.println("**********************************************");
    }

    private static Rectangle initRectangle(){
        System.out.print("Enter 2 sides of rectangle: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        return new Rectangle(a, b);
    }

    private static Triangle initTriangle(){
        System.out.print("Enter 3 sides of triangle: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        return new Triangle(a, b, c);
    }

    private static Circle initCircle(){
        System.out.print("Enter radius of circle: ");
        int r = sc.nextInt();
        return new Circle(r);
    }
}
