package com.epam.shapes;

public class Triangle extends Shape {
    private int a;
    private int b;
    private int c;

    public Triangle(int a, int b, int c) {
        super(3);
        if ((a > 0) && (b > 0) && (c > 0)) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }

    public double getSquare() {
        double p = getPerimeter()/2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public double getPerimeter() {
        return a + b + c;
    }

    public double getHeight() {
        return (2. / a) * getSquare();
    }
}
