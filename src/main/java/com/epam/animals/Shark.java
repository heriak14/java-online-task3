package com.epam.animals;

public class Shark extends Fish{
    private String species;

    public Shark(String species) {
        super(false, "fish", true);
        this.species = species;
    }

    public void getInfo() {
        System.out.println("****************************************");
        System.out.println("Here you can see a" + species + " shark!");
        System.out.println("It is a " + (isSeaFish() ? "sea" : "river") + " fish.");
        System.out.println("Shark is a " + (isVegetarian() ? "vegetarian" : "meat-eating") + " animal.");
        System.out.println("It usually eats " + getEats());
        System.out.println("****************************************\n");
    }
}
