package com.epam.animals;

import java.util.Scanner;

public class AnimalManager {
    private static Animal animal;

    public static void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome in our zoo!");
        System.out.println("What kind of animal you want to see?:");
        System.out.println("Bird - 1\nFish - 2");
        switch (sc.nextInt()) {
            case 1:
                animal = new Parrot("red", "Ara");
                animal.getInfo();
                animal = new Eagle("brown", "hawk");
                animal.getInfo();
                break;
            case 2:
                animal = new Amour("White");
                animal.getInfo();
                animal = new Shark("Tiger");
                animal.getInfo();
                break;
            default :
                System.out.println("You have intered incorect data!");
                System.exit(0);

        }
        System.out.println("Thank you for visiting our zoo");
    }
}
