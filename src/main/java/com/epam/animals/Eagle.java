package com.epam.animals;

public class Eagle extends Bird {
    private String color;
    private String species;

    public Eagle(String color, String species) {
        super(true, "meat", true);
        this.color = color;
        this.species = species;
    }

    public void getInfo() {
        System.out.println("****************************************");
        System.out.println("Here you can see a" + color + " " + species + " eagle!");
        System.out.println("It is a " + (isFlyable() ? "flyable" : "not flyable") + " bird.");
        System.out.println("Parrot is a " + (isVegetarian() ? "vegetarian" : "meat-eating") + " animal.");
        System.out.println("It usually eats " + getEats());
        System.out.println("****************************************\n");
    }
}
