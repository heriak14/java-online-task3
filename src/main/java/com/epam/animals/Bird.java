package com.epam.animals;

public abstract class Bird extends Animal {
    private boolean flyable;

    public Bird(boolean vegetarian, String food, boolean flyable) {
        super(2, vegetarian, food);
        this.flyable = flyable;
    }

    public boolean isFlyable() {
        return flyable;
    }

    public void setFlyable(boolean flyable) {
        this.flyable = flyable;
    }
}
