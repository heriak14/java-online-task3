package com.epam.animals;

public class Amour extends Fish {
    private String species;

    public Amour(String species) {
        super(true, "algae", false);
        this.species = species;
    }

    public void getInfo() {
        System.out.println("****************************************");
        System.out.println("Here you can see a" + species + " amour!");
        System.out.println("It is a " + (isSeaFish() ? "sea" : "river") + " fish.");
        System.out.println("Amour is a " + (isVegetarian() ? "vegetarian" : "meat-eating") + " animal.");
        System.out.println("It usually eats " + getEats());
        System.out.println("****************************************\n");
    }
}
