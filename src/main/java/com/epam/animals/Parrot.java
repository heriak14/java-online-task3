package com.epam.animals;

public class Parrot extends Bird{
    private String color;
    private String species;

    public Parrot(String color, String species) {
        super(true, "plants", true);
        this.color = color;
        this.species = species;
    }

    public void getInfo() {
        System.out.println("****************************************");
        System.out.println("Here you can see a" + color + " " + species + " parrot!");
        System.out.println("It is a " + (isFlyable() ? "flyable" : "not flyable") + " bird.");
        System.out.println("Parrot is a " + (isVegetarian() ? "vegetarian" : "meat-eating") + " animal.");
        System.out.println("It usually eats " + getEats());
        System.out.println("****************************************\n");
    }
}
