package com.epam.animals;

public abstract class Animal {
    private int numOfLegs;
    private boolean vegetarian;
    private String eats;

    public Animal(int numOfLegs, boolean vegetarian, String food) {
        this.numOfLegs = numOfLegs;
        this.vegetarian = vegetarian;
        this.eats = food;
    }
    public int getNumOfLegs() {
        return numOfLegs;
    }

    public void setNumOfLegs(int numOfLegs) {
        this.numOfLegs = numOfLegs;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public String getEats() {
        return eats;
    }

    public void setEats(String eats) {
        this.eats = eats;
    }

    public abstract void getInfo();

}
