package com.epam.animals;

public abstract class Fish extends Animal {
    private boolean seaFish;

    public Fish(boolean vegetarian, String food, boolean seaFish) {
        super(0, vegetarian, food);
        this.seaFish = seaFish;
    }

    public boolean isSeaFish() {
        return seaFish;
    }

    public void setSeaFish(boolean seaFish) {
        this.seaFish = seaFish;
    }
}
